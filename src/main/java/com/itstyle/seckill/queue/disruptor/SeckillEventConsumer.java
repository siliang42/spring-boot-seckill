package com.itstyle.seckill.queue.disruptor;

import com.itstyle.seckill.common.config.SpringUtil;
import com.itstyle.seckill.service.ISeckillService;
import com.lmax.disruptor.EventHandler;
import org.springframework.stereotype.Component;

/**
 * 消费者(秒杀处理器)
 * 创建者
 */
public class SeckillEventConsumer implements EventHandler<SeckillEvent> {
	
	private ISeckillService seckillService = (ISeckillService) SpringUtil.getBean("seckillService");
	
	@Override
	public void onEvent(SeckillEvent seckillEvent, long seq, boolean bool) throws Exception {
		seckillService.startSeckil(seckillEvent.getSeckillId(), seckillEvent.getUserId());
	}
}
